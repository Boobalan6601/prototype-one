import React, { Component } from 'react';
import DesignDetail from './DesignDetails';
import Header from './HeaderComponent';
import Footer from './FooterComponent'; 
import Home from './HomeComponent';
import About from './AboutComponent';
import DesignMenu from './DesignComponent';
import { Switch, Route, Redirect,withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import CarouselItem from './CarouselOne';
import {fetchDishes,postComment, fetchComments} from '../redux/ActionCreators';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const mapStateToProps = state => {

  return{
    comments: state.comments,
    dishes: state.dishes,

  }
}

const mapDispatchToProps = (dispatch) => ({
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
    fetchComments: () => dispatch(fetchComments()),
    fetchDishes: () => { dispatch(fetchDishes()) }
 

});

class Main extends Component {

  constructor(props) {
    super(props);


  }
  
  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
  } 
 
  render() {
    const HomePage = () => {
        return(
            <Home/>
        );
      }

    const AboutUsPage = () => {
        return(
            <About/>
        );
      }
      const DishWithId = ({match}) => {
        return(
            <DesignDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
                isLoading={this.props.dishes.isLoading}
                errMess={this.props.dishes.errMess}
                
                comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))}
                commentsErrMess={this.props.comments.errMess}
                postComment={this.props.postComment}
            />
        );
    };

      return(
      <div>
          
   
              <Switch>
              <Route path='/home' component={HomePage} />
              {/*<Route exact path='/menu' component={() => <DesignMenu dishes={this.state.design} />} />*/}
              <Route exact path="/aboutus" component={ AboutUsPage } />
              <Route exact path="/design" component={ () => <DesignMenu dishes={this.props.dishes} /> } />
              <Route path="/design/:dishId" component={DishWithId} />
              <Redirect to="/home" />
              </Switch>
       
        <Footer />
      </div>
  );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));