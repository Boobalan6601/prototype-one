import React, { Component } from 'react';
import {
    Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron,
    Button, Modal, ModalBody, ModalHeader, Form, FormGroup, Label, Input
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Sidebar from './sidebar';



const JumbotronItem = props => {

    return (
        <Jumbotron>
            <div className="container">
                <div className="row row-header">
                    <div className="col-12 col-sm-6">
                        <style>
                            @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&family=Hammersmith+One&family=Lato:wght@700&family=Merriweather+Sans&display=swap');
                        </style>
                        <h1>{props.title}</h1>
                        <span className='accent-bar-top'></span>
                        <p>{props.description}</p>
                    </div>
                </div>
            </div>
        </Jumbotron>
    );
}
class Header extends Component {
    constructor(props) {
        super(props);

        this.toggleNav = this.toggleNav.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this)

        this.state = {
            isNavOpen: false,
            isModalOpen: false
        };
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }


    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleLogin(event) {
        this.toggleModal();

        alert("Username: " + this.username.value + " password: " + this.password.value +
            " remember: " + this.remember.checked)
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <React.Fragment>
                    <Navbar dark expand="md fixed-top">
                        <div className="container">
                            <Sidebar />
                            <NavbarToggler onClick={this.toggleNav} />
                            <NavbarBrand className="ml-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='First Company' />First Company</NavbarBrand>
                            <Collapse isOpen={this.state.isNavOpen} navbar>
                                <Nav navbar>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/design'><span className="fa fa-user fa-lg"></span> Designs</NavLink>
                                    </NavItem>
                                </Nav>s
                                <Nav navbar>
                                    <NavItem className="ml-auto">
                                        <Button outline onClick={this.toggleModal} >
                                            <span className='fa fa-sign-in' > Login</span></Button>
                                    </NavItem>
                                </Nav>

                            </Collapse>
                        </div>
                    </Navbar>
                    <Jumbotron >
                        <div className="container">
                            <div className="row row-header">
                                <div className="col-12 col-sm-6">
                                    <style>
                                        @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&family=Hammersmith+One&family=Lato:wght@700&family=Merriweather+Sans&display=swap');
                                    </style>
                                    <h1>{this.props.title}</h1>
                                    <span className='accent-bar-top'></span>
                                    <p>{this.props.description}</p>
                                </div>
                            </div>
                        </div>
                    </Jumbotron>

                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>Login Form</ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.handleLogin}>
                                <FormGroup>
                                    <Label htmlFor='username'>User Name</Label>
                                    <Input type='text' id='username' name='username' placeholder="username"
                                        innerRef={(input) => this.username = input}></Input>
                                </FormGroup>

                                <FormGroup>
                                    <Label htmlFor='password'>Password</Label>
                                    <Input type='password' id='password' name='password' placeholder="password"
                                        innerRef={(input) => this.password = input}></Input>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type='checkbox' name='remember'
                                            innerRef={(input) => this.remember = input}></Input>
                                        Remember me
                                    </Label>

                                </FormGroup>
                                <Button type='submit' value='submit' color='info'>Submit</Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </React.Fragment>
            </div>
        );
    }
}

export default Header;