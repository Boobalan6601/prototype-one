import React, { Component } from 'react';
import {
    Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron,
    Button, Modal, ModalBody, ModalHeader, Form, FormGroup, Label, Input
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Sidebar from './sidebar';



const mystyle = {
    padding:'200px 0px 70px 70px',
    color:'floralwhite',
    height: '470px',
    backgroundImage: 'url("https://news-cdn.softpedia.com/images/news2/6-Meters-Tall-3D-Printer-Can-Build-Houses-Out-of-Regular-Soil-Video-460585-2.jpg")',
    backgroundSize: '100% 100%',
    fontFamily:  "roboto",
    fontWeight: '400'
  };


  const style1={
      backgroundColor: '#E9ECEF'
  }
class About extends Component {
    constructor(props) {
        super(props);

        this.toggleNav = this.toggleNav.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this)

        this.state = {
            isNavOpen: false,
            isModalOpen: false
        };
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }


    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleLogin(event) {
        this.toggleModal();

        alert("Username: " + this.username.value + " password: " + this.password.value +
            " remember: " + this.remember.checked)
        event.preventDefault();
    }

render(){
return(
    <>
           <Navbar dark expand="md fixed-top">
                        <div className="container">
                            <Sidebar/>
                            <NavbarToggler onClick={this.toggleNav} />
                            <NavbarBrand className="ml-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='First Company' /></NavbarBrand>
                            <Collapse isOpen={this.state.isNavOpen} navbar>
                                <Nav navbar>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/design'><span className="fa fa-user fa-lg"></span> Designs</NavLink>
                                    </NavItem>
                                </Nav>
                                <Nav navbar>
                                    <NavItem className="ml-auto">
                                        <Button outline onClick={this.toggleModal} >
                                            <span className='fa fa-sign-in' > Login</span></Button>
                                    </NavItem>
                                </Nav>

                            </Collapse>
                        </div>
                    </Navbar>



        <div style={mystyle}>
            <div className="row about-header">
                <div className="col-12 col-sm-6">
                <style>
@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap');
</style>
                    <h1>We are a team of strategists, designers, architects, and engineers</h1>
                    <span className='accent-bar-top'></span>
                    
                </div>
            </div>
        </div>
    


<div>
    <div className='container'>
        <div className='row about-content'>
               <h1>About First Company</h1>
               <p>First Company is a next-gen startup developed by a group of six friends to revolutionize the atmosphere of the market by integrating the service industry with the product industry aspiring to create a space where creators and customers coexist.</p>
        </div>
    </div>
</div>

<div>
    <div className='container'>
        <div className='row about-middle'>
            <div className='col-12 col-md-7'>
                <img src={"https://www.theburnin.com/wp-content/uploads/2019/09/3d-printing-innovations.png"} height='350px' width='450px' ></img>
            </div>
            <div className='col-12 col-md-5'>
                <h1>Innovation</h1>
                <div>
                    <span className='accent-bar'></span>
                </div>
                <h2>We dare to design the future of 3D world !</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
            </div>
        </div>
    </div>
        <div style={style1}>
        <div className='row about-middle'>
            <div className='col-12 col-md-5'>
            <h1>Our Mission</h1>
                <div>
                    <span className='accent-bar'></span>
                </div>
                <h2>We dare to design the future of 3D world !</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
            </div>
            <div className='col-12 col-md-7'>
            <img src={"https://www.theburnin.com/wp-content/uploads/2019/09/3d-printing-innovations.png"} height='350px' width='450px' ></img>

            </div>
        </div>
        </div> 
        <div className='container'>
        <div className='row about-middle'>
            <div className='col-12 col-md-7'>
                <img src={"https://www.theburnin.com/wp-content/uploads/2019/09/3d-printing-innovations.png"} height='350px' width='450px' ></img>
            </div>
            <div className='col-12 col-md-5'>
                <h1>Our Team</h1>
                <div>
                    <span className='accent-bar'></span>
                </div>
                <h2>We dare to design the future of 3D world !</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
            </div>
        </div>
    </div>
</div>

<div className='container'>
        <div className='row about-content'>
               <h1>What We do</h1>
               <p>First Company is a next-gen startup developed by a group of six friends to revolutionize the atmosphere of the market by integrating the service industry with the product industry aspiring to create a space where creators and customers coexist.</p>
        </div>
</div>


<div>
    <div className='row about-bottom transparentbox'>
        <div className='transparentText'>
            <h1>REACH US TO GET YOUR CUSTOMIZED PRODUCTS</h1>
        </div>
    </div>
</div>
<div className='container'>
    <div className='row row-content'>

    </div>
</div>


    </>
)

}

}

export default About;
