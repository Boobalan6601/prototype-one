
import React from "react";
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';


const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 }
];

function ImageSlider({ dish, onClick }) {
  return (
    <div>
      <Carousel className='Slider' breakPoints={breakPoints}>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5f1a62d942a6387efb759310%2F0x0.jpg'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">{dish.name}</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">{dish.description}</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
     </Carousel>
    </div>
  );
}

const CarouselItem = (props) => {

    const menu = props.dishes.map((dish) => {
        return (
            <div className="col-12 col-md-5 m-1" key={dish.id}>
                <ImageSlider dish={dish} onClick={props.onClick} />
            </div>
        );
    });

    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <h3>Menu</h3>
                    <hr />
                </div>
            </div>
            <div className="row">
                {menu}
            </div>
        </div>
    );
}

export default CarouselItem;

