import React from "react";
import ReactDOM from "react-dom";
import model from '../img/model.svg';



function Flow(){
    return(
    <React.Fragment>
        <div className='container'>
        <div className='row process-outline'>
            <div className="process col-12 col-md-3">
                <img src={model} height="50px" width="50px" alt="model"></img>
                
                <div className='process-content'>
                <h5>Choose your design</h5>
                </div>
            
            </div>
            {/* <div className='col-md-1'>
                <span className='accent-bar'></span>
                </div> */}
            <div className="process justify-content-center col-12 col-md-3">
                <img src={model}  height="50px" width="50px" alt="model"></img> 
                <div className='process-content'>
                <h5>Choose your design</h5>
                </div>
            </div>
          {/*   <div className='col-md-1'>
                <span className='accent-bar'></span>
                </div> */}
            <div className="process justify-content-center col-12 col-md-3">
                <img src={model}  height="50px" width="50px" alt="model"></img> 
                <div className='process-content'>
                <h5>Choose your design</h5>
                </div>
            </div>
           {/*  <div className='col-md-1'>
            <span className='accent-bar'></span>
            </div> */}
            <div className="process justify-content-center col-12 col-md-3">
                <img src={model}  height="50px" width="50px" alt="model"></img> 
                <div className='process-content'>
                <h5>Choose your design</h5>
                </div>
            </div>
        </div>
        </div>
        
    </React.Fragment>
    )
}

export default Flow;

 


/* import React from 'react';




import {Button,ProgressIndicator,ProgressStep} from 'react-rainbow-components';

const stepNames = ['step-1', 'step-2', 'step-3', 'step-4', 'step-5'];

const steps = ['first', 'second', 'third', 'fourth', 'fifth'];

export default class SimpleProgressStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStepIndex: 0,
        };
        this.handleNextClick = this.handleNextClick.bind(this);
        this.handleBackClick = this.handleBackClick.bind(this);
    }

    handleNextClick() {
        const { currentStepIndex } = this.state;
        if (currentStepIndex < stepNames.length - 1) {
            const nextStepIndex = currentStepIndex + 1;
            return this.setState({ currentStepIndex: nextStepIndex });
        }
        return this.setState({ isNextDisabled: false });
    }

    handleBackClick() {
        const { currentStepIndex } = this.state;
        if (currentStepIndex > 0) {
            const previewStepIndex = currentStepIndex - 1;
            this.setState({ currentStepIndex: previewStepIndex });
        }
    }

    isNextDisabled() {
        const { currentStepIndex } = this.state;
        if (currentStepIndex < stepNames.length - 1 && currentStepIndex >= 0) {
            return false;
        }
        return true;
    }

    isBackDisabled() {
        const { currentStepIndex } = this.state;
        if (currentStepIndex > 0 && currentStepIndex < stepNames.length) {
            return false;
        }
        return true;
    }

    render() {
        const { currentStepIndex, isBackDisabled, isNextDisabled } = this.state;
        return (
            <div style={{width: 1000}} className="rainbow-m-bottom_large rainbow-p-bottom_large">
                <ProgressIndicator currentStepName={stepNames[currentStepIndex]}>
                    <ProgressStep name="step-1" />
                    <ProgressStep name="step-2" />
                    <ProgressStep name="step-3" />
                    <ProgressStep name="step-4" />
                    <ProgressStep name="step-5" />
                </ProgressIndicator>
                <div className="rainbow-m-top_xx-large rainbow-align-content_center rainbow-flex_wrap">
                    <p>{`This is the ${steps[currentStepIndex]} step`}</p>
                </div>
                <div className="rainbow-m-top_xx-large rainbow-align-content_center rainbow-flex_wrap">
                    <Button
                        label="Back"
                        onClick={this.handleBackClick}
                        variant="neutral"
                        disabled={this.isBackDisabled()}
                        className="rainbow-m-horizontal_medium"
                    />
                    <Button
                        label="Next"
                        onClick={this.handleNextClick}
                        variant="brand"
                        disabled={this.isNextDisabled()}
                        className="rainbow-m-horizontal_medium"
                    />
                </div>
            </div>
        );
    }
} */