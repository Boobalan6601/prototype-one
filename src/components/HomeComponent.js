import React from 'react';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Example from './CarouselComponent';
import CarouselPage from './CarouselComponent';
import ImageSlider from './CarouselComponent';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import Flow from './Flowdia';
import Carousel from 'react-bootstrap/Carousel';


const CardHome = props => {

    return (
        <Card className="text-center card">
            <div className="overflow">
                <CardImg src={props.image} width="250px" height="200px" className="card-img-top" />
            </div>
            <CardBody className="card-body text-dark">
                <CardTitle>{props.title}</CardTitle>
                <CardText className="card-text text-secondary">
                    {props.cardtext}

                </CardText>
                <a href="#" className="btn btn-outline-success">Learn More</a>
            </CardBody>
        </Card>
    );
}

function Home(props) {
    return (
        <>

                <Header className="homebgimg" title="THE FUTURE OF MANUFACTURING" description="We take inspiration from the World's
                     best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!"
                />



            <div>
                <div className='top'>
                    <div className='row row-content'>

                        <div className='top-heading'>
                            <strong><h1>Ac placerat vestibulum lectus mauris</h1></strong>
                        </div>
                        <div className='top-content'>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At consectetur lorem donec massa sapien faucibus et. Eu turpis egestas pretium aenean pharetra magna ac placerat. Laoreet suspendisse interdum consectetur libero. Tristique senectus et netus et.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='row row-content'>
                    <div className="col-12 col-md-4 top-card">
                        <Card className='top-card-item'>
                            <div className="overflow">
                                <CardImg src={'https://i.insider.com/60b93a6f89dbc300189b291a?width=1000&format=jpeg&auto=webp'} width="300px" height="200px" className="card-img-top" />
                            </div>
                            <CardBody className="card-body text-dark">
                                <CardTitle><h3>OUR PRODUCTS</h3></CardTitle>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-12 col-md-4 top-card">
                        <Card className='top-card-item'>
                            <div className="overflow">
                                <CardImg src={'https://d12oja0ew7x0i8.cloudfront.net/images/Article_Images/ImageForArticle_20017_161044277420545.jpg'} width="300px" height="200px" className="card-img-top" />
                            </div>
                            <CardBody className="card-body text-dark">
                                <CardTitle><h3>OUR SERVICES</h3></CardTitle>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-12 col-md-4 top-card">
                        <Card className='top-card-item'>
                            <div className="overflow">
                            </div>
                            <CardImg src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7UMZYfHAxNtXZhiB-KGycR1XTA0eJahYnmg&usqp=CAU'} width="300px" height="200px" className="card-img-top" />

                            <CardBody className="card-body text-dark">
                                <CardTitle><h3>ABOUT FIRST COMPANY</h3></CardTitle>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
            <div>
                <div className='container'>
                    <div className='row top-carousel'>
                        <div className='row top-carousel-quick col-md-4'>

                            <p>QUICK LINKS</p>
                            <div>
                                <span className='accent-bar'></span>
                            </div>
                            <div className='top-carousel-links'>EXPLORE OUR STRATEGY </div>
                            <div className='top-carousel-links'>FIND LOCATIONS </div>
                            <div className='top-carousel-links'>LEARNING RESOURCES </div>
                            <div className='top-carousel-links'>FIND US ON SOCIAL </div>
                            <style>
                                @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&family=Hammersmith+One&family=Lato:wght@700&family=Merriweather+Sans&display=swap');
                            </style>
                        </div>
                        <div className=' top-carousel-slide col-12 col-md-8'>
                            <Carousel fade>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src="https://i.insider.com/60b93a6f89dbc300189b291a?width=1000&format=jpeg&auto=webp"
                                        alt="First slide"
                                        height='400px'
                                        width='400px'
                                    />
                                    <Carousel.Caption>
                                        <h3>First slide label</h3>
                                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src="https://base.imgix.net/files/base/ebm/industryweek/image/2020/01/3d_printing.5e13a2f970369.png?auto=format&fit=max&w=1200"
                                        alt="Second slide"
                                        height='400px'
                                        width='300px'
                                    />

                                    <Carousel.Caption>
                                        <h3>Second slide label</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src="https://d12oja0ew7x0i8.cloudfront.net/images/Article_Images/ImageForArticle_20017_161044277420545.jpg"
                                        alt="Third slide"
                                        height='400px'
                                        width='300px'
                                    />

                                    <Carousel.Caption>
                                        <h3>Third slide label</h3>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                            </Carousel>
                        </div>
                    </div>
                    <br></br>
                </div>
            </div>
            <hr></hr>

            <div className='middle-content'>
                <br></br>
                <div className='row'>
                    <div className=' middle-content-left col-12 col-md-7' >
                        <div className='middle-content-heading'><strong><h1>sed turpis</h1></strong></div>
                        <div>
                            <span className='accent-bar'></span>
                        </div>
                        <h2>adipiscing elit pellentesque</h2>
                        <h2>habitant morbi </h2>
                        <h2>tristique senectus et</h2>
                        <br></br>
                    </div>
                    <div className='col-12 col-md-5' >
                        <div className='middle-content-right'>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis convallis convallis tellus id. In nulla posuere sollicitudin aliquam ultrices. In nisl nisi scelerisque eu ultrices vitae auctor eu augue. Aliquet porttitor lacus luctus accumsan tortor. Ut ornare lectus sit amet est placerat.
                                Suspendisse potenti nullam ac tortor vitae. </p>
                            <p>Et ultrices neque ornare aenean euismod elementum nisi. Lectus quam id leo in vitae turpis massa sed. Amet massa vitae tortor condimentum lacinia quis vel. Vel facilisis volutpat est velit egestas dui. Sit amet porttitor eget dolor. In fermentum et sollicitudin ac orci phasellus egestas tellus.
                                Aliquet sagittis id consectetur purus ut faucibus pulvinar. Scelerisque varius morbi enim nunc faucibus. Volutpat sed cras ornare arcu. Commodo elit at imperdiet dui. Ac tortor dignissim convallis aenean et. Ac orci phasellus egestas tellus rutrum tellus pellentesque. Id cursus metus aliquam eleifend mi in nulla posuere.</p>
                        </div>
                        <br></br>
                    </div>

                </div>
            </div>
            <hr></hr>

            <div className="container">
                <div className='row row-content justify-content-center'>
                    <h1>Our products</h1>
                    <div>
                        <span className='accent-bar'></span>
                    </div>

                </div>


            </div>
            <ImageSlider />
            <hr></hr>
            <div className="container">
                <div className='row row-content'>
                    <h1 className='justify-content-center'>How it works</h1>
                    <div>
                        <span className='accent-bar'></span>
                    </div>

                    <Flow />
                </div>

                {/*<SimpleProgressStep />*/}
            </div>

            <div className="container">
                <div className='row row-content justify-content-center'>
                    <h1>Our Products</h1>
                    <div>
                        <span className='accent-bar'></span>
                    </div>

                </div>


            </div>




            <div className=" container container-fluid d-flex justify-content-center">
                <div className="row">
                    <div className="col-md-4 card-bottom">
                        <CardHome image={'https://base.imgix.net/files/base/ebm/industryweek/image/2020/01/3d_printing.5e13a2f970369.png?auto=format&fit=max&w=1200'}
                            title="3d-printer Model 1" cardtext=" this one of the best machine at affordable cost, 
         this machine works on the principle of Fluid deposition modeling"/>
                    </div>
                    <div className="col-md-4 card-bottom">
                        <CardHome image={'https://i.insider.com/60b93a6f89dbc300189b291a?width=1000&format=jpeg&auto=webp'}
                            title="3d-printer Model 2" cardtext=" this one of the best machine at affordable cost, 
         this machine works on the principle of Fluid deposition modeling"/>
                    </div>
                    <div className="col-md-4 card-bottom">
                        <CardHome image={'https://d12oja0ew7x0i8.cloudfront.net/images/Article_Images/ImageForArticle_20017_161044277420545.jpg'}
                            title="3d-printer Model 3" cardtext=" this one of the best machine at affordable cost, 
         this machine works on the principle of Fluid deposition modeling"/>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='row row-content'>

                </div>
            </div>


        </>

    );
}

export default Home;


