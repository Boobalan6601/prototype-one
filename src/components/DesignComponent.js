import React from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button,Breadcrumb, BreadcrumbItem
  } from 'reactstrap';
  import {Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron} from 'reactstrap';
  import { NavLink } from 'react-router-dom';
  import Sidebar from './sidebar';

import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import Header from './HeaderComponent';


 function ImageSlider({ dish, onClick }) {
    return (
      <div>
      
        <div className="card-item">
        <Card>
        <Link to={ `/design/${dish.id}` } >
        <CardImg width="250px" height="200px" src={baseUrl + dish.image} alt={dish.name} />
          <CardBody>
            <CardTitle tag="h5">{dish.name}</CardTitle>
            <CardSubtitle tag="h6" className="mb-2 text-muted">{dish.description}</CardSubtitle>
            <Button>Button</Button>
          </CardBody>
          </Link>
        </Card>
        </div>
   
      </div>
    );
  }
  

const DesignMenu = (props) => {

    const menu = props.dishes.dishes.map((dish) => {
        return (
            <div className="col-12 col-md-5 m-1" key={dish.id}>
               <ImageSlider dish={dish} onClick={props.onClick} />
            </div>
        );
    });

    if (props.dishes.isLoading) {
        return(
            <>
           
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Menu</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Menu</h3>
                        <hr />
                    </div>
                </div>
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
                 </div>
                 </>
        );
    }
    if (props.dishes.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.dishes.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.dishes.errMess}</h4>
                </div>
            </div>
        );
    }
    else
    {
        return(
            <div>
            <Header/>
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home">Home</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>
                            Menu
                        </BreadcrumbItem>
                    </Breadcrumb>
                    
                    <div className="col-12">
                        <h3>Menu</h3>
                        <hr />
                    </div>
                </div>

                <div className="row">
                    { menu }
                </div>
            </div>

            </div>
        );

    }


}

export default DesignMenu;