
import React from "react";
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';


const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 }
];

function ImageSlider() {
  return (
    <div>
      <Carousel className='Slider' breakPoints={breakPoints}>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5f1a62d942a6387efb759310%2F0x0.jpg'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">hello</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-8DXWDV-bVxCveFYO64YiRTxXxMbIwtnq10GTy6pLxrGOysbRJ3_pe-JFCzdGo5MQ_mc&usqp=CAU'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
       </div>
       <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px"src={'https://base.imgix.net/files/base/ebm/industryweek/image/2020/01/3d_printing.5e13a2f970369.png?auto=format&fit=max&w=1200'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5f1a62d942a6387efb759310%2F0x0.jpg'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://i.insider.com/60b93a6f89dbc300189b291a?width=1000&format=jpeg&auto=webp'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://www.sigmaaldrich.com/deepweb/assets/sigmaaldrich/marketing/global/images/applications/materials-science-and-engineering-/stereolithography-photopolymer-3D-printer/stereolithography-photopolymer-3D-printer.jpg'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPS91QFIaXddTsVUF7JAMhjFYNr0AV-vyzS1zWFTM9wa2mofV6yrYGi5-iNIn5ZC0C2pI&usqp=CAU'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      </div>
      <div className="card-item">
      <Card>
        <CardImg top width="300px" height="200px" src={'https://d12oja0ew7x0i8.cloudfront.net/images/Article_Images/ImageForArticle_20017_161044277420545.jpg'} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>  
      </div>

      </Carousel>
    </div>
  );
}

export default ImageSlider;

