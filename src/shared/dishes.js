export const DISHES =
    [
        {
        id: 0,
        name:'3D printer 1',
        image: 'assets/images/3d1.jpg',
        category: 'mains',
        label:'Hot',
        price:'4.99',
        description:'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'
        },
        {
        id: 1,
        name:'3D printer 2',
        image: 'assets/images/3d2.png',
        category: 'appetizer',
        label:'',
        price:'1.99',
        description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'

        },
        {
        id: 2,
        name:'3D printer 3',
        image: 'assets/images/3d3.jpg',
        category: 'appetizer',
        label:'New',
        price:'1.99',
        description:'A quintessential ConFusion experience, is it a vada or is it a donut?'

        },
        {
        id: 3,
        name:'3D printer 4',
        image: 'assets/images/3d4.jpg',
        category: 'dessert',
        label:'',
        price:'2.99',
        description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
   
        },
        {
            id: 4,
            name:'3D printer 4',
            image: 'assets/images/3d4.jpg',
            category: 'dessert',
            label:'',
            price:'2.99',
            description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
       
        }
    ];